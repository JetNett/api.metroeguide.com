﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using api.metroeguide.com.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace api.metroeguide.com.Services
{
    public class DapperService
    {
        private static NpgsqlConnection _conn;
        private static NpgsqlCommand _comm;

        public DapperService(IHostingEnvironment env, IConfiguration config)
        {
            var ConnectionString = config["ConnectionStrings:Dev"] ?? string.Empty; // _config.GetConnectionString("PostgresLSDev") ?? string.Empty;
            _conn = new NpgsqlConnection(ConnectionString);
            _comm = _conn.CreateCommand();
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        public List<T> GetData<T>(string query, string connectionString = null)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = _conn.ConnectionString;
            }

            var response = new List<T>();

            using (var conn = new Npgsql.NpgsqlConnection(connectionString))
            {
                response = conn.Query<T>(query).ToList();
            }

            return response == null ? null : response;
        }
    }
}
