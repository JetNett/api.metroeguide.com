﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace api.metroeguide.com.Models
{
    [DataContract]
    public class CustomerRequest
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int? Limit { get; set; }

        [DataMember(Order = 3)]
        public int Skip { get; set; }

        [DataMember(Order = 4)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 5)]
        public bool OrderByDesc { get; set; }

        [DataMember(Order = 6)]
        public bool Condensed { get; set; }

        [DataMember(Order = 7)]
        public int ClientId { get; set; }

    }

    [DataContract]
    public class CustomerResponse
    {
        [DataMember(Order = 1)]
        public List<CustomerDto> Result { get; set; }

        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }

        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }

        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }

    public class DeleteCustomerRequest
    {
        public int Id { get; set; }
    }

}

