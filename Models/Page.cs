﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace api.metroeguide.com.Models
{
    [DataContract]
    public class PageDto
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int FolderId { get; set; }

        [DataMember(Order = 3)]
        public string Title { get; set; }

        [DataMember(Order = 4)]
        public bool AutoOrdering { get; set; }

        [DataMember(Order = 5)]
        public string FooterHtml { get; set; }

        [DataMember(Order = 6)]
        public string HeaderHtml { get; set; }

        [DataMember(Order = 7)]
        public string MetaKeys { get; set; }

        [DataMember(Order = 8)]
        public string MetaDesc { get; set; }

        [DataMember(Order = 9)]
        public string Route { get; set; }

        [DataMember(Order = 10)]
        public string CanonicalUrl { get; set; }

        [DataMember(Order = 11)]
        public string Comments { get; set; }

        [DataMember(Order = 12)]
        public string Zipcode { get; set; }

        [DataMember(Order = 13)]
        public DateTime Modified { get; set; }

    }

    public class PageResponse
    {
        [DataMember(Order = 1)]
        public List<PageDto> Result { get; set; }
        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }
        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }
        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }

    public class PageRequest
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int FolderId { get; set; }

        [DataMember(Order = 3)]
        public string Title { get; set; }

        [DataMember(Order = 4)]
        public string Route { get; set; }

        [DataMember(Order = 5)]
        public string CanonicalUrl { get; set; }

        [DataMember(Order = 6)]
        public int? Limit { get; set; }

        [DataMember(Order = 7)]
        public int? Skip { get; set; }

        [DataMember(Order = 8)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 9)]
        public bool OrderByDesc { get; set; }

        [DataMember(Order = 10)]
        public string Zipcode { get; set; }
    }
}
