﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace api.metroeguide.com.Models
{
    [DataContract]
    public class LinkRequest
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public int UrlId { get; set; }
        [DataMember(Order = 3)]
        public int PageId { get; set; }
        [DataMember(Order = 4)]
        public string Title { get; set; }
        [DataMember(Order = 5)]
        public string Url { get; set; }
        [DataMember(Order = 6)]
        public int? Limit { get; set; }
        [DataMember(Order = 7)]
        public bool? IsSuspended { get; set; }
        [DataMember(Order = 8)]
        public bool? IsElevated { get; set; }
        [DataMember(Order = 9)]
        public int Skip { get; set; }
        [DataMember(Order = 10)]
        public string[] OrderBy { get; set; }
        [DataMember(Order = 11)]
        public bool OrderByDesc { get; set; }
    }

    public class LinkDto
    {
        //[AutoIncrement]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int UrlId { get; set; }

        [DataMember(Order = 3)]
        public string Title { get; set; }

        [DataMember(Order = 4)]
        public string Url { get; set; }

        [DataMember(Order = 5)]
        public bool IsLink { get; set; }

        [DataMember(Order = 6)]
        public int PageId { get; set; }

        [DataMember(Order = 7)]
        public int? Position { get; set; }

        [DataMember(Order = 8)]
        public string Target { get; set; }

        [DataMember(Order = 9)]
        public string Comments { get; set; }

        [DataMember(Order = 10)]
        public bool? IsSuspended { get; set; }

        [DataMember(Order = 11)]
        public bool? IsElevated { get; set; }

        [DataMember(Order = 12)]
        public DateTime Modified { get; set; }
    }

    public class LinkResponse
    {
        [DataMember(Order = 1)]
        public List<LinkDto> Result { get; set; }
        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }
        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }
        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }
}
