﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace api.metroeguide.com.Models
{
    public class CommunityProfile
    {
    }

    public class CommunityProfileDto
    {
        public int ClientId { get; set; }
        public int PageId { get; set; }
    }

    [DataContract]
    public class iLinkCommunityProfileResponse
    {
        [DataMember(Order = 1)]
        public List<PageDto> Result { get; set; }

        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }

        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
    }

    [DataContract]
    public class iLinkCommunityProfileRequest
    {
        [DataMember(Order = 1)]
        public int ClientId { get; set; }
        [DataMember(Order = 4)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 5)]
        public bool OrderByDesc { get; set; }
    }
}
