﻿using System;
using System.Runtime.Serialization;

namespace api.metroeguide.com.Models
{
    [DataContract]
    public class CustomerDto
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public decimal ClientId { get; set; }

        [DataMember(Order = 3)]
        public string Name { get; set; }

        [DataMember(Order = 4)]
        public string UserId { get; set; }

        [DataMember(Order = 5)]
        public string Password { get; set; }

        [DataMember(Order = 6)]
        public string Css { get; set; }

        [DataMember(Order = 7)]
        public string Comments { get; set; }

        [DataMember(Order = 8)]
        public DateTime Modified { get; set; }

        [DataMember(Order = 9)]
        public string BackAnchorText { get; set; }

        [DataMember(Order = 10)]
        public string BackAnchorUrl { get; set; }

        [DataMember(Order = 11)]
        public string BackAnchorTarget { get; set; }

        [DataMember(Order = 12)]
        public string CommunityProfileIds { get; set; }

        [DataMember(Order = 13)]
        public string ClientLogoImageSrc { get; set; }

        [DataMember(Order = 14)]
        public string ClientLogoImageHref { get; set; }

        [DataMember(Order = 15)]
        public string ClientLogoImageTarget { get; set; }

        [DataMember(Order = 16)]
        public string ClientLogoImageAlt { get; set; }

        [DataMember(Order = 17)]
        public string ProductLogoImageSrc { get; set; }

        [DataMember(Order = 18)]
        public string ProductLogoImageHref { get; set; }

        [DataMember(Order = 19)]
        public string ProductLogoImageAlt { get; set; }

        [DataMember(Order = 20)]
        public string ProductLogoImageTarget { get; set; }

        [DataMember(Order = 21)]
        public string MainTitle { get; set; }

        [DataMember(Order = 22)]
        public string CenterGraphic { get; set; }

        [DataMember(Order = 23)]
        public string CenterGraphicLinkUrl { get; set; }

        [DataMember(Order = 24)]
        public string TopLeftGraphicUrl { get; set; }

        [DataMember(Order = 25)]
        public string TopLeftGraphicTarget { get; set; }

        [DataMember(Order = 26)]
        public int LeftButtonCount { get; set; }

        [DataMember(Order = 27)]
        public int LeftButtonLinkPageId { get; set; }

        [DataMember(Order = 28)]
        public string MegGraphicUrl { get; set; }

        [DataMember(Order = 29)]
        public string SearchFolders { get; set; }

        [DataMember(Order = 30)]
        public int ButtonCount { get; set; }

        [DataMember(Order = 31)]
        public string ButtonLinkPageId { get; set; }

        [DataMember(Order = 32)]
        public string CompanyGraphicUrl { get; set; }

        [DataMember(Order = 33)]
        public string CompanyName { get; set; }

        [DataMember(Order = 34)]
        public string Email { get; set; }

        [DataMember(Order = 35)]
        public string PhoneNumber { get; set; }

        [DataMember(Order = 36)]
        public string PhoneNumber2 { get; set; }

        [DataMember(Order = 37)]
        public string FaxNumber { get; set; }

        [DataMember(Order = 38)]
        public string StreetAddress { get; set; }

        [DataMember(Order = 39)]
        public string City { get; set; }

        [DataMember(Order = 40)]
        public string State { get; set; }

        [DataMember(Order = 41)]
        public string Zip { get; set; }

        [DataMember(Order = 42)]
        public string OtherInformation { get; set; }

        [DataMember(Order = 43)]
        public string MobileLogoUrl { get; set; }

        [DataMember(Order = 44)]
        public string MobileUrl { get; set; }

        [DataMember(Order = 45)]
        public string MobileGraphicPhoneNumber { get; set; }

        [DataMember(Order = 46)]
        public string HomePageTitle { get; set; }

        [DataMember(Order = 47)]
        public string SeoMetaKeys { get; set; }

        [DataMember(Order = 48)]
        public string SeoMetaDesc { get; set; }

        [DataMember(Order = 49)]
        public string AnalyticsKey { get; set; }

        [DataMember(Order = 50)]
        public string PageBackgroundColor { get; set; }

        [DataMember(Order = 51)]
        public string MainTitleFontFamily { get; set; }

        [DataMember(Order = 52)]
        public int MainTitleFontSize { get; set; }

        [DataMember(Order = 53)]
        public string MainTitleFontColor { get; set; }

        [DataMember(Order = 54)]
        public string CenterGraphicBorderColor { get; set; }

        [DataMember(Order = 55)]
        public int CenterGraphicBorderThickness { get; set; }

        [DataMember(Order = 56)]
        public int CopyrightFontSize { get; set; }

        [DataMember(Order = 57)]
        public string CopyrightFontColor { get; set; }

        [DataMember(Order = 58)]
        public string CopyrightFontFamily { get; set; }

        [DataMember(Order = 59)]
        public string RealtorGraphicUrl { get; set; }

        [DataMember(Order = 60)]
        public int ButtonBorderThickness { get; set; }

        [DataMember(Order = 61)]
        public string ButtonBorderColor { get; set; }

        [DataMember(Order = 62)]
        public string ButtonTitleFontColor { get; set; }

        [DataMember(Order = 63)]
        public string ButtonTitleFontFamily { get; set; }

        [DataMember(Order = 64)]
        public string ButtonTitleFontWeight { get; set; }

        [DataMember(Order = 65)]
        public string ButtonBackgroundColor { get; set; }

        [DataMember(Order = 66)]
        public string ButtonMouseoverBgColor { get; set; }

        [DataMember(Order = 67)]
        public string ButtonMouseoverBorderColor { get; set; }

        [DataMember(Order = 68)]
        public string ButtonMouseoverFontColor { get; set; }

        [DataMember(Order =69)]
        public int RightButtonCount { get; set; }

        [DataMember(Order = 70)]
        public int RightButtonLinkPageId { get; set; }

    }
}