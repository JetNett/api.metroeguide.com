﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using api.metroeguide.com.Models;
using api.metroeguide.com.Services;

namespace api.metroeguide.com.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunityProfileController : ControllerBase
    {
        private static IHostingEnvironment _env;
        private static IConfiguration _config;
        private static DapperService _dapperServ;
        public CommunityProfileController(IHostingEnvironment env, IConfiguration config)
        {
            if (env != null)
            {
                _env = env;
            }
            else
            {
                throw new ArgumentNullException("env", "Hosting env was null.");
            }

            _config = config;

            _dapperServ = new DapperService(_env, _config);
        }

        [HttpGet("{clientId}")]
        public iLinkCommunityProfileResponse GetCommunityProfile(int clientId)
        {
            //var sw = Stopwatch.StartNew();
            
            var commProfiles = _dapperServ.GetData<int>($"select page_id from community_profiles_mappings where client_id = {clientId}")
                .ToList();

            //request.OrderBy = request.OrderBy ?? new[] { "Title" };

            var expression = $"select * from pages ORDER BY Title";

            //expression += request.OrderByDesc
            //    ? throw new NotImplementedException("Descending order not available")
            //    : request.OrderBy.ToString();

            var result = _dapperServ.GetData<PageDto>(expression)
                .Where(x => commProfiles.Contains(x.Id))
                .ToList();

            var response = new iLinkCommunityProfileResponse(); //request.ConvertTo<iLinkCommunityProfileResponse>();
            response.Result = result;
            //response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }
    }
}
