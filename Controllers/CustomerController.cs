﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Npgsql;
using api.metroeguide.com.Models;
using api.metroeguide.com.Services;

namespace api.metroeguide.com.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _config;
        private NpgsqlConnection _conn;
        private NpgsqlCommand _comm;
        protected CustomerDto _customer;

        public CustomerController(IHostingEnvironment env, IConfiguration config)
        {
            if (env != null)
            {
                _env = env;
            }
            else
            {
                throw new ArgumentNullException("env", "Hosting env was null.");
            }

            _config = config;

            var ConnectionString = _config["ConnectionStrings:Dev"] ?? string.Empty; // _config.GetConnectionString("PostgresLSDev") ?? string.Empty;
            _conn = new NpgsqlConnection(ConnectionString);
            _comm = _conn.CreateCommand();
        }

        // GET api/values?value1
        [HttpGet]
        public CustomerResponse CustomerByClientIdQuery([FromQuery (Name = "clientId")] int clientId)
        {
            return CustomerByRowOrClient(new CustomerRequest { ClientId = clientId });
        }

        // GET api/values/5
        [HttpGet("{clientId:int}")]
        public CustomerResponse CustomerByClientId(int clientId)
        {
            //var newp = clientId;
            return CustomerByRowOrClient(new CustomerRequest { ClientId = clientId });
        }

        // GET api/values
        [HttpGet("{CustomerRequest}")]
        public CustomerResponse CustomerByRowOrClient([FromBody] CustomerRequest request)
        {
            var result = default(List<CustomerDto>);

            request.OrderBy = request.OrderBy ?? new[] { "Name" };
            request.Limit = request.Limit ?? 100;

            if (request.Id != default(int) || request.ClientId != default(int))
            {
                //result = new[] { Db.Single<CustomerDto>(x => x.Id == request.Id) }
                var query = string.Empty;
                if (request.Id != default(int))
                {
                    query = $"Select * from public.customers where id = {request.Id}"; //because the Model is not correct, get data manually
                }
                else
                {
                    query = $"Select * from public.customers where clientID = {request.ClientId}"; //because the Model is not correct, get data manually
                }

                result = new DapperService(_env, _config).GetData<CustomerDto>(query, _conn.ConnectionString);
            }
            else if (request.Condensed)
            {
                var condensedQuery = "SELECT id, clientid, name, modified FROM Customers ORDER BY name ASC";
                using (var db = new NpgsqlConnection(_conn.ConnectionString))
                {
                    //result = db.Query<CustomerDto>(condensedQuery).ToList();   //this returns all properties, only some as non-null
                    result = SqlMapper.Query<CustomerDto>(db, condensedQuery).ToList();
                }
            }

            var response = new CustomerResponse(); //request.ConvertTo<CustomerResponse>();
            response.Result = result;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }






        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
