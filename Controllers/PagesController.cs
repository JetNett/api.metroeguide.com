﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using api.metroeguide.com.Models;
using api.metroeguide.com.Services;

namespace api.metroeguide.com.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagesController : ControllerBase
    {
        private static IHostingEnvironment _env;
        private static IConfiguration _config;
        private static DapperService _dapperServ;
        public PagesController(IHostingEnvironment env, IConfiguration config)
        {
            if (env != null)
            {
                _env = env;
            }
            else
            {
                throw new ArgumentNullException("env", "Hosting env was null.");
            }

            _config = config;

            _dapperServ = new DapperService(_env, _config);
        }

        //[HttpGet]
        //public ActionResult Index() => new BadRequestResult();

        //controller?param=val is treated as controller with nothing after it
        [HttpGet]
        public PageResponse GetPagesByQueryId([FromQuery(Name = "id")] int id)
        {
            return GetPagesById(id);
        }

        [HttpGet("{id:int}")]
        public PageResponse GetPagesById(int id)
        {
            if (id < 1) return new PageResponse();

            //return GetPages(new PageRequest { Id = Convert.ToInt32(id) });
            return GetPages(new PageRequest { Id = id });
        }

        [HttpGet("{PageRequest}")]
        public PageResponse GetPages(PageRequest request)
        {
            //var sw = Stopwatch.StartNew();

            var result = new List<PageDto>();
            
            request.OrderBy = request.OrderBy ?? new[] { "Title" };
            request.Limit = request.Limit ?? 100;
            
            if (request.Id != default)
            {
                result = _dapperServ.GetData<PageDto>($"Select * from pages where Id = {request.Id}")
                    .ToList();
            }
            else if (request.FolderId != default(int))
            {
                result = _dapperServ.GetData<PageDto>($"Select * from pages where folder_id = {request.FolderId}");
                    //.Where(x => x.PageId == request.PageId);
            }
            else if (request.Route != default(string))
            {
                //todo: refactor for speed and add criteria to initial statement
                result = (List<PageDto>)_dapperServ.GetData<PageDto>($"Select * from pages LIMIT {request.Limit}")
                    .Where(x =>
                        (x.Route == request.Route || request.Route == null)
                        &&
                        (x.CanonicalUrl == request.CanonicalUrl || request.CanonicalUrl == null)
                        &&
                        (x.Title == request.Title || request.Title == null)
                        &&
                        (x.Zipcode == request.Zipcode || request.Zipcode == null));
            }

            //expression = request.OrderByDesc
            //    ? expression.OrderByFieldsDescending(request.OrderBy)
            //    : expression.OrderByFields(request.OrderBy);

            //todo: enable this -- expression = expression.Limit(request.Skip, request.Limit);

            //if (result == default(List<LinkDto>))
            //{
            //    result = Db.Select<LinkDto>(expression);
            //}

            var response = new PageResponse
            {
                Result = result,
                ResultCount = result.Count
            }; 
            response.Result = result;
            //response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //cache the respone
            //var cacheObj = request.ToProtoBufCacheObj(response);
            //Redis.StringSet(requestKey, (RedisValue)cacheObj, TimeSpan.FromDays(14));

            return response;
        }
    }
}
