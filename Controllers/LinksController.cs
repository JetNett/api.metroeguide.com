﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using api.metroeguide.com.Models;
using api.metroeguide.com.Services;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace api.metroeguide.com.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinksController : ControllerBase
    {
        private static IHostingEnvironment _env;
        private static IConfiguration _config;
        private static DapperService _dapperServ;
        public LinksController(IHostingEnvironment env, IConfiguration config)
        {
            if (env != null)
            {
                _env = env;
            }
            else
            {
                throw new ArgumentNullException("env", "Hosting env was null.");
            }

            _config = config;

            _dapperServ = new DapperService(_env, _config);
        }

        public ActionResult<string> Index()
        {
            return "Incomplete Request";
        }

        [HttpGet]
        public LinkResponse GetLinksQuery([FromQuery] int pageId, string[] orderBy = null, bool isSuspended = false, bool isElevated = false)
        {
            return (GetLinks(new LinkRequest
            {
                PageId = pageId,
                OrderBy = orderBy,
                IsSuspended = isSuspended,
                IsElevated = isElevated
            }));
        }

        [HttpGet("getlinks/{LinkRequest}")]
        public LinkResponse GetLinks(LinkRequest request)
        {
            //var sw = Stopwatch.StartNew();

            var result = new List<LinkDto>();
            string sortFields = request.OrderBy == null || request.OrderBy.Length < 1 ? "Page_Id, Position, Title, Url" : "Title";
            request.OrderBy = request.OrderBy ?? new[] { "Page_Id", "Position", "Title", "Url" };
            request.Limit = request.Limit ?? 100;
            
            //var expression = Db.From<LinkDto>();

            if (request.Id != default)
            {
                result = _dapperServ.GetData<LinkDto>($"Select * from links where Id = {request.Id} LIMIT {request.Limit}")
                    .ToList();
            }
            else if (request.PageId != default(int))
            {
                result = _dapperServ.GetData<LinkDto>($"Select * from links where page_id = {request.PageId} order by {sortFields} LIMIT {request.Limit} ");
                    //.Where(x => x.PageId == request.PageId);
            }
            else if (request.UrlId != default(int))
            {
                result = _dapperServ.GetData<LinkDto>($"Select * from links where url_id = {request.UrlId}"); //.Where(x => x.UrlId == request.UrlId);
            }
            else if (request.Title != null && request.Url != null)
            {
                result = _dapperServ.GetData<LinkDto>($"Select * from links where title = {request.Title} and url = {request.Url}");
                    //.Where(x => x.Title == request.Title && x.Url == request.Url);
            }
            else if ((request.Title ?? request.Url) != null || (request.IsElevated ?? request.IsSuspended) != null)
            {
                //todo: refactor for speed and add criteria to initial statement
                result = (List<LinkDto>)_dapperServ.GetData<LinkDto>($"Select * from links LIMIT {request.Limit}")
                    .Where(x =>
                        (x.Title == request.Title || request.Title == null)
                        &&
                        (x.Url == request.Url || request.Url == null)
                        &&
                        (x.IsElevated == request.IsElevated || request.IsElevated == null)
                        &&
                        (x.IsSuspended == request.IsSuspended || request.IsSuspended == null));
            }

            //expression = request.OrderByDesc
            //    ? expression.OrderByFieldsDescending(request.OrderBy)
            //    : expression.OrderByFields(request.OrderBy);

            //todo: enable this -- expression = expression.Limit(request.Skip, request.Limit);

            //if (result == default(List<LinkDto>))
            //{
            //    result = Db.Select<LinkDto>(expression);
            //}

            var response = new LinkResponse
            {
                Result = result,
                ResultCount = result.Count
            }; 
            response.Result = result;
            //response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //cache the respone
            //var cacheObj = request.ToProtoBufCacheObj(response);
            //Redis.StringSet(requestKey, (RedisValue)cacheObj, TimeSpan.FromDays(14));

            return response;
        }
        
        [HttpGet("getwikidata/{WikiArticleID}")]
        public string GetWikiData(string wikiArticleID)
        {
            if (String.IsNullOrEmpty(wikiArticleID)) return "";

            //context.Response.ContentType = "application/json";
            //context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            int.TryParse(wikiArticleID, out int wikiID);
            if (wikiID <= 0)
            {
                return "";
            }

            var html = "";
            try
            {
                //belleville 151302
                var myRequest =
                (HttpWebRequest) WebRequest.Create(string.Format("http://en.wikipedia.org/w/api.php?action=query&prop=extracts&pageids={0}&format=json&exintro=1", wikiID));
                System.Net.ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; //todo: remove for production
                myRequest.UserAgent = "Phil B (gmphil2008@gmail.com)";
                using (var response = (HttpWebResponse)myRequest.GetResponse())
                {
                    string responseText;

                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseText = reader.ReadToEnd();
                    }

                    var jsonReader = new JsonTextReader(new StringReader(responseText));
                    while (jsonReader.Read())
                    {
                        if (jsonReader.TokenType == JsonToken.PropertyName)
                        {
                            if ((string)jsonReader.Value == "extract")
                            {
                                jsonReader.Read();
                                html = (string)jsonReader.Value;
                            }
                        }

                    }
                }
            }
            catch (Exception b1) { return ($"WikiData: {wikiArticleID} Error block 1: {b1.Message}"); }

            try
            {
                var fullURLRequest = (HttpWebRequest)WebRequest.Create(string.Format("http://en.wikipedia.org/w/api.php?action=query&prop=info&format=json&pageids={0}&inprop=url", wikiID));
                System.Net.ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                fullURLRequest.UserAgent = "Phil B (gmphil2008@gmail.com)";
                using (var response = (HttpWebResponse)fullURLRequest.GetResponse())
                {
                    string responseText;
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseText = reader.ReadToEnd();
                    }

                    var jsonReader = new JsonTextReader(new StringReader(responseText));
                    var fullURL = "";
                    while (jsonReader.Read())
                    {
                        if (jsonReader.TokenType == JsonToken.PropertyName)
                        {
                            if ((string)jsonReader.Value == "fullurl")
                            {
                                jsonReader.Read();
                                fullURL = (string)jsonReader.Value;
                            }
                        }
                    }

                    if (html.Length > 9)
                        html = html.Insert(html.IndexOf("</p>", System.StringComparison.Ordinal), string.Format("<span style=\"font-size:8pt\"> (<a href=\"{0}\" style=\"color:blue\" target=\"_blank\">Wikipedia.org</a>)</span>", fullURL));

                    var firstEnd = html.IndexOf("</p>", System.StringComparison.Ordinal);
                    if (firstEnd > 0)
                        html = html.Substring(0, firstEnd + 4);

                    return "{ \"extract\" :\"" + html.Replace("\"", "'").Replace("\n", "") + "\" }";
                }
            }
            catch (Exception b2) { return "Error block 2: " + b2.Message; }
        }
    }
}
